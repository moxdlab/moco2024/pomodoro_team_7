package com.pomo.myapplication.todo

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.pomo.myapplication.timerroom.TaskEntity


@Composable
fun ToDoScreen(onBackClick: () -> Unit, taskViewModel: TaskViewModel) {
    val tasks by taskViewModel.allTasks.collectAsState(initial = emptyList())  // Flow als State beobachten
    var showDialog by remember { mutableStateOf(false) }

    Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
        Column(
            modifier = Modifier.fillMaxSize().padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = onBackClick) {
                    Icon(imageVector = Icons.AutoMirrored.Filled.ArrowBack, contentDescription = "Back")
                }
                Spacer(modifier = Modifier.weight(1f))
                Text(text = "To-Do", style = MaterialTheme.typography.headlineLarge, modifier = Modifier.align(Alignment.CenterVertically))
                Spacer(modifier = Modifier.weight(1f))
                IconButton(onClick = { showDialog = true }) {
                    Icon(imageVector = Icons.Default.Add, contentDescription = "Add Task")
                }
            }

            Spacer(modifier = Modifier.height(20.dp))

            LazyColumn {
                items(tasks) { taskEntity ->
                    val task = Task(taskEntity.description, taskEntity.time, taskEntity.isCompleted)
                    TaskItem(
                        task = task,
                        onComplete = { completedTask ->
                            taskViewModel.updateTask(taskEntity.copy(isCompleted = !completedTask.isCompleted))
                        },
                        onRemove = {
                            taskViewModel.deleteTask(taskEntity)
                        }
                    )
                }
            }
        }

        if (showDialog) {
            TaskInputDialog(
                onDismiss = { showDialog = false },
                onAddTask = { taskText, taskTime ->
                    val newTaskEntity = TaskEntity(description = taskText, time = taskTime, isCompleted = false)
                    taskViewModel.insertTask(newTaskEntity)
                    showDialog = false
                }
            )
        }
    }
}



@Composable
fun TaskItem(task: Task, onComplete: (Task) -> Unit, onRemove: (Task) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .border(1.dp, Color.Black)
            .background(Color.White)
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column(modifier = Modifier.weight(1f)) {
            Text(
                text = task.description,
                style = MaterialTheme.typography.bodyLarge,
                color = if (task.isCompleted) Color.Gray else Color.Black
            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = "Time: ${task.time} min",
                style = MaterialTheme.typography.bodyMedium,
                color = Color.Gray
            )
        }
        IconButton(onClick = { onComplete(task) }) {
            Icon(
                imageVector = if (task.isCompleted) Icons.Filled.Refresh else Icons.Filled.Check,
                contentDescription = if (task.isCompleted) "Undo Completion" else "Complete Task"
            )
        }
        IconButton(onClick = { onRemove(task) }) {
            Icon(
                imageVector = Icons.Filled.Delete,
                contentDescription = "Remove Task"
            )
        }
    }
}

@Composable
fun TaskInputDialog(onDismiss: () -> Unit, onAddTask: (String, String) -> Unit) {
    var taskText by remember { mutableStateOf("") }
    var taskTime by remember { mutableStateOf("") }

    Dialog(onDismissRequest = onDismiss) {
        Surface(
            modifier = Modifier.padding(16.dp),
            color = MaterialTheme.colorScheme.surface,
            shape = MaterialTheme.shapes.medium
        ) {
            Column(
                modifier = Modifier.padding(16.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Text(text = "Add New Task", style = MaterialTheme.typography.titleMedium)

                // Task Field with Label and Border
                Column(modifier = Modifier.fillMaxWidth()) {
                    Text(text = "Task", style = MaterialTheme.typography.bodyMedium)
                    Spacer(modifier = Modifier.height(4.dp))
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .border(1.dp, Color.Black)
                            .padding(4.dp)
                    ) {
                        BasicTextField(
                            value = taskText,
                            onValueChange = { taskText = it },
                            modifier = Modifier.fillMaxWidth(),
                            textStyle = MaterialTheme.typography.bodyLarge.copy(color = Color.Black)
                        )
                    }
                }

                // Time Field with Label, Border and Numeric Input
                Column(modifier = Modifier.fillMaxWidth()) {
                    Text(text = "Time (minutes)", style = MaterialTheme.typography.bodyMedium)
                    Spacer(modifier = Modifier.height(4.dp))
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .border(1.dp, Color.Black)
                            .padding(4.dp)
                    ) {
                        BasicTextField(
                            value = taskTime,
                            onValueChange = { newValue ->
                                // Only allow numeric input
                                if (newValue.all { it.isDigit() }) {
                                    taskTime = newValue
                                }
                            },
                            modifier = Modifier.fillMaxWidth(),
                            textStyle = MaterialTheme.typography.bodyLarge.copy(color = Color.Black)
                        )
                    }
                }

                Row(
                    horizontalArrangement = Arrangement.End,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = "Cancel",
                        color = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.clickable(onClick = onDismiss)
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(
                        text = "Add",
                        color = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.clickable {
                            onAddTask(taskText, taskTime)
                        }
                    )
                }
            }
        }
    }
}

data class Task(
    val description: String,
    val time: String,
    val isCompleted: Boolean
)