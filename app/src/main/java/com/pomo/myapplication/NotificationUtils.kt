package com.pomo.myapplication

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Handler
import android.os.Looper
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class NotificationHelper(private val context: Context) {

    companion object {
        const val PERMISSION_REQUEST_CODE = 1001
        const val CHANNEL_ID = "timer_channel"
        const val NOTIFICATION_ID = 0
        const val ACTION_STOP_TIMER = "com.pomo.myapplication.ACTION_STOP_TIMER"
    }

    val handler = Handler(Looper.getMainLooper())
    var timeLeft = "00:00"
    var isTimerRunning = false

    init {
        createNotificationChannel()
        registerStopReceiver()
    }

    fun createNotificationChannel() {

        val name = "Timer Notification"
        val descriptionText = " "
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
            description = descriptionText
            enableLights(true)
            enableVibration(true)
            vibrationPattern = longArrayOf(1000, 1000)
        }
        val notificationManager: NotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    fun startTimerCountdown(durationInMillis: Long) {
        timeLeft = formatTime(durationInMillis)
        isTimerRunning = true
        updateNotification(NotificationCompat.PRIORITY_LOW, playSound = false)

        handler.postDelayed(object : Runnable {
            var remainingTime = durationInMillis

            override fun run() {
                if (isTimerRunning && remainingTime > 0) {
                    remainingTime -= 1000
                    timeLeft = formatTime(remainingTime)
                    updateNotification(NotificationCompat.PRIORITY_LOW, playSound = false)
                    handler.postDelayed(this, 1000)
                } else {
                    isTimerRunning = false
                    showTimerFinishedNotification() // Sound when timer finishes

                }
            }
        }, 0)
    }

    fun updateNotification(priority: Int, playSound: Boolean) {
        if (!hasNotificationPermission()) return

        val notification = buildNotification(
            title = "Timer läuft",
            contentText = "Noch verbleibende Zeit: $timeLeft",
            priority = priority,
            playSound = playSound
        )

        showNotification(notification)
    }

    fun showTimerFinishedNotification() {
        if (!hasNotificationPermission()) return

        val notification = buildNotification(
            title = "Timer abgelaufen",
            contentText = "Der Timer ist abgelaufen!",
            priority = NotificationCompat.PRIORITY_MAX,
            playSound = true
        )

        showNotification(notification)
    }

    fun cancelNotification() {
        isTimerRunning = false
        handler.removeCallbacksAndMessages(null)
        with(NotificationManagerCompat.from(context)) {
            cancel(NOTIFICATION_ID)
        }
    }

    @SuppressLint("DefaultLocale")
    fun formatTime(durationInMillis: Long): String {
        val minutes = (durationInMillis / 1000) / 60
        val seconds = (durationInMillis / 1000) % 60
        return String.format("%02d:%02d", minutes, seconds)
    }

    fun hasNotificationPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            true
        } else {
            if (context is Activity) {
                ActivityCompat.requestPermissions(
                    context,
                    arrayOf(Manifest.permission.POST_NOTIFICATIONS),
                    PERMISSION_REQUEST_CODE
                )
            }
            false
        }
    }

    fun buildNotification(title: String, contentText: String, priority: Int, playSound: Boolean): Notification {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)

        return NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(contentText)
            .setPriority(priority)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .apply {
                if (!playSound) {
                    setSilent(true)
                }
            }
            .build()
    }

    fun showNotification(notification: Notification) {
        try {
            with(NotificationManagerCompat.from(context)) {
                notify(NOTIFICATION_ID, notification)
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    fun registerStopReceiver() {
        val filter = IntentFilter(ACTION_STOP_TIMER)
        context.registerReceiver(TimerStopReceiver(), filter, Context.RECEIVER_EXPORTED)
    }


    class TimerStopReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            if (intent?.action == ACTION_STOP_TIMER) {
                val notificationHelper = NotificationHelper(context)
                notificationHelper.cancelNotification()
            }
        }
    }

}
