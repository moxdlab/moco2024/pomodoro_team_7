package com.pomo.myapplication.timer

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.List
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.pomo.myapplication.MainActivity
import com.pomo.myapplication.NotificationHelper

@SuppressLint("DefaultLocale")
@Composable
fun TimerScreen(
    onToDoListClick: () -> Unit,
    notificationHelper: NotificationHelper,
    activity: MainActivity,
    viewModel: SharedViewModel
) {

    val timeLeftInMillis by viewModel.timeLeftInMillis.observeAsState(25 * 60 * 1000L)
    val timerRunning by viewModel.timerRunning.observeAsState(false)
    val inputMinutes by viewModel.inputMinutes.observeAsState("")
    val inputSeconds by viewModel.inputSeconds.observeAsState("")
    val showDialog by viewModel.showDialog.observeAsState(false)

    val timeFormatted by remember(timeLeftInMillis) {
        derivedStateOf {
            val minutes = (timeLeftInMillis / 1000) / 60
            val seconds = (timeLeftInMillis / 1000) % 60
            String.format("%02d:%02d", minutes, seconds)
        }
    }

    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 80.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            TimerDisplay(time = timeFormatted)
            Spacer(modifier = Modifier.height(20.dp))
            Row {
                Button(
                    onClick = { viewModel.startTimer(notificationHelper, activity, timeFormatted) },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xFF00AA07),
                        contentColor = Color.White
                    ),
                    enabled = !timerRunning
                ) {
                    Text(text = "Start")
                }

                Spacer(modifier = Modifier.width(8.dp))
                Button(
                    onClick = { viewModel.stopTimer(notificationHelper) },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xFFD32F2F),
                        contentColor = Color.White
                    ),
                    enabled = timerRunning
                ) {
                    Text(text = "Pause")
                }
                Spacer(modifier = Modifier.width(8.dp))
                Button(onClick = { viewModel.resetTimer(notificationHelper) }) {
                    Text(text = "Reset")
                }
            }

            Spacer(modifier = Modifier.height(20.dp))
            Row {
                Button(
                    onClick = { viewModel.setTimer(5, 0, notificationHelper) },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xFF6650a4),
                        contentColor = Color.White
                    )
                ) {
                    Text(text = "5 min")
                }

                Spacer(modifier = Modifier.width(8.dp))
                Button(
                    onClick = { viewModel.setTimer(15, 0, notificationHelper) },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xFF6650a4),
                        contentColor = Color.White
                    )
                ) {
                    Text(text = "15 min")
                }

                Spacer(modifier = Modifier.width(8.dp))
                Button(
                    onClick = { viewModel.setTimer(25, 0, notificationHelper) },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xFF6650a4),
                        contentColor = Color.White
                    )
                ) {
                    Text(text = "25 min")
                }
            }

            Spacer(modifier = Modifier.height(20.dp))
            Button(
                onClick = { viewModel.showDialog() },
                colors = ButtonDefaults.buttonColors(
                    containerColor = Color.Magenta,
                    contentColor = Color.White
                )
            ) {
                Text(text = "Set Custom Timer")
            }

            if (showDialog) {
                CustomTimerDialog(
                    inputMinutes = inputMinutes,
                    inputSeconds = inputSeconds,
                    onMinutesChange = { viewModel.updateInputMinutes(it) },
                    onSecondsChange = { viewModel.updateInputSeconds(it) },
                    onConfirm = { viewModel.setCustomTimer(notificationHelper) },
                    onDismiss = { viewModel.hideDialog() }
                )
            }
        }

        FloatingActionButton(
            onClick = onToDoListClick,
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(16.dp)
        ) {
            Icon(
                imageVector = Icons.AutoMirrored.Filled.List,
                contentDescription = "To-Do List"
            )
        }
    }
}



@Composable
fun TimerDisplay(time: String) {
    Text(
        text = time,
        style = MaterialTheme.typography.displayLarge
    )
}
