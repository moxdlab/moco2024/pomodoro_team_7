package com.pomo.myapplication.timer

import android.Manifest
import android.app.Application
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pomo.myapplication.MainActivity
import com.pomo.myapplication.NotificationHelper
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SharedViewModel(application: Application) : AndroidViewModel(application) {

    private val _timeLeftInMillis = MutableLiveData(25 * 60 * 1000L)
    val timeLeftInMillis: LiveData<Long> = _timeLeftInMillis

    private val _timerRunning = MutableLiveData(false)
    val timerRunning: LiveData<Boolean> = _timerRunning

    private val _inputMinutes = MutableLiveData("")
    val inputMinutes: LiveData<String> = _inputMinutes

    private val _inputSeconds = MutableLiveData("")
    val inputSeconds: LiveData<String> = _inputSeconds

    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean> = _showDialog

    private var lastSelectedDuration = 25

    fun startTimer(notificationHelper: NotificationHelper, activity: MainActivity, timeFormatted: String) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.POST_NOTIFICATIONS)
            == PackageManager.PERMISSION_GRANTED
        ) {
            _timerRunning.value = true
            notificationHelper.startTimerCountdown(_timeLeftInMillis.value ?: 0L)

            viewModelScope.launch {
                while (_timeLeftInMillis.value ?: 0L > 0 && _timerRunning.value == true) {
                    delay(1000L)
                    _timeLeftInMillis.value = _timeLeftInMillis.value?.minus(1000L)
                    activity.timeLeftForNotification = timeFormatted
                    if ((_timeLeftInMillis.value ?: 0L) % 60000L == 0L) {
                        notificationHelper.updateNotification(NotificationCompat.PRIORITY_MIN, playSound = false)
                    }
                }
                if (_timeLeftInMillis.value ?: 0L <= 0) {
                    _timerRunning.value = false
                    notificationHelper.showTimerFinishedNotification()
                }
            }
        } else {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(Manifest.permission.POST_NOTIFICATIONS),
                MainActivity.PERMISSION_REQUEST_CODE
            )
        }
    }

    fun stopTimer(notificationHelper: NotificationHelper) {
        _timerRunning.value = false
        notificationHelper.cancelNotification()
    }


    fun resetTimer(notificationHelper: NotificationHelper) {
        _timeLeftInMillis.value = lastSelectedDuration * 60 * 1000L
        _timerRunning.value = false
        notificationHelper.cancelNotification()
    }

    fun setTimer(minutes: Int, seconds: Int = 0, notificationHelper: NotificationHelper) {
        if (minutes > 0 || seconds > 0) {
            _timeLeftInMillis.value = (minutes * 60 * 1000L) + (seconds * 1000L)
            lastSelectedDuration = minutes + seconds
            _timerRunning.value = false
            notificationHelper.cancelNotification()
        }
    }

    fun setCustomTimer(notificationHelper: NotificationHelper) {
        val minutes = _inputMinutes.value?.toIntOrNull()?.coerceAtLeast(0) ?: 0
        val seconds = _inputSeconds.value?.toIntOrNull()?.coerceAtLeast(0) ?: 0
        if (minutes > 0 || seconds > 0) {
            setTimer(minutes, seconds, notificationHelper)
            _showDialog.value = false
        }
    }

    fun updateInputMinutes(value: String) {
        _inputMinutes.value = value
    }

    fun updateInputSeconds(value: String) {
        _inputSeconds.value = value
    }

    fun showDialog() {
        _showDialog.value = true
    }

    fun hideDialog() {
        _showDialog.value = false
    }
}