package com.pomo.myapplication.timer

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.width

@Composable
fun CustomTimerDialog(
    inputMinutes: String,
    inputSeconds: String,
    onMinutesChange: (String) -> Unit,
    onSecondsChange: (String) -> Unit,
    onConfirm: () -> Unit,
    onDismiss: () -> Unit
) {
    AlertDialog(
        onDismissRequest = onDismiss,
        title = { Text(text = "Set Custom Timer") },
        text = {
            Column {
                Text(text = "Enter the number of minutes and seconds:")
                Spacer(modifier = Modifier.height(8.dp))
                Row {
                    TextField(
                        value = inputMinutes,
                        onValueChange = onMinutesChange,
                        modifier = Modifier.weight(1f).width(120.dp),
                        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                        placeholder = { Text(text = "Minutes") }
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    TextField(
                        value = inputSeconds,
                        onValueChange = onSecondsChange,
                        modifier = Modifier.weight(1f).width(120.dp),
                        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                        placeholder = { Text(text = "Seconds") }
                    )
                }
            }
        },
        confirmButton = {
            Button(onClick = onConfirm) {
                Text(text = "OK")
            }
        },
        dismissButton = {
            Button(onClick = onDismiss) {
                Text(text = "Cancel")
            }
        }
    )
}
