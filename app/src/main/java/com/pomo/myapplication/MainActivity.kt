package com.pomo.myapplication

import android.Manifest
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.pomo.myapplication.timer.TimerScreen
import com.pomo.myapplication.todo.TaskViewModel
import com.pomo.myapplication.todo.ToDoScreen
import com.pomo.myapplication.ui.theme.MyApplicationTheme

class MainActivity : ComponentActivity() {

    companion object {
        const val PERMISSION_REQUEST_CODE = 1001
    }

    var timeLeftForNotification: String = ""
    lateinit var notificationHelper: NotificationHelper
    lateinit var permissionsHelper: PermissionsHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initialisere Notification- und PermissionHelper
        notificationHelper = NotificationHelper(this)
        permissionsHelper = PermissionsHelper(this)

        val postNotificationLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                permissionsHelper.handlePermissionResult(
                    Manifest.permission.POST_NOTIFICATIONS,
                    isGranted
                )
            }

        val accessNotificationPolicyLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                permissionsHelper.handlePermissionResult(
                    Manifest.permission.ACCESS_NOTIFICATION_POLICY,
                    isGranted
                )
            }

        val useExactAlarmLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                permissionsHelper.handlePermissionResult(
                    Manifest.permission.USE_EXACT_ALARM,
                    isGranted
                )
            }

        // Initialisiere permission launchers mit Launcher
        permissionsHelper.initializePermissionLaunchers(
            postNotificationLauncher = postNotificationLauncher,
            accessNotificationPolicyLauncher = accessNotificationPolicyLauncher,
            useExactAlarmLauncher = useExactAlarmLauncher
        )

        // Check and request permissions
        permissionsHelper.checkAndRequestPermissions()

        // Set up the UI content
        setContent {
            MyApplicationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    SetupNavGraph(navController = navController)
                }
            }
        }
    }


    @Composable
    fun SetupNavGraph(navController: NavHostController) {
        // ViewModel einmalig für beide Screens
        val taskViewModel: TaskViewModel = viewModel()

        NavHost(
            navController = navController,
            startDestination = "timer_screen"
        ) {
            composable("timer_screen") {
                TimerScreen(
                    onToDoListClick = {
                        // Navigiere zum To-Do-Screen und fordere ausstehende Berechtigungen an
                        navController.navigate("todo_screen")
                        permissionsHelper.requestPendingPermissions()
                    },
                    notificationHelper = notificationHelper,
                    activity = this@MainActivity,
                    viewModel = viewModel()
                )
            }
            composable("todo_screen") {
                // Übergib den ViewModel an die ToDoScreen
                ToDoScreen(
                    onBackClick = { navController.navigateUp() },
                    taskViewModel = taskViewModel
                )
            }
        }
    }
}
