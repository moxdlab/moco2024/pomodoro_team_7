package com.pomo.myapplication.ui.theme



import kotlin.random.Random

object MotivationalQuotes {
    private val quotes = listOf(
        "Believe in yourself!",
        "Keep pushing forward!",
        "You can do it!",
        "Stay positive and work hard!",
        "Success is not final, failure is not fatal: It is the courage to continue that counts."
    )

    fun getRandomQuote(): String {
        return quotes[Random.nextInt(quotes.size)]
    }
}
