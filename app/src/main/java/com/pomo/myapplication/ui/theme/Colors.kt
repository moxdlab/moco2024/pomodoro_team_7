package com.pomo.myapplication.ui.theme

import androidx.compose.ui.graphics.Color


val LightBackground = Color(0xFFFFFBFE)
val LightSurface = Color(0xFFFFFBFE)
val LightOnPrimary = Color.White
val LightOnSecondary = Color.White
val LightOnTertiary = Color.White
val LightOnBackground = Color(0xFF1C1B1F)
val LightOnSurface = Color(0xFF1C1B1F)


val DarkBackground = Color(0xFF121212)
val DarkSurface = Color(0xFF121212)
val DarkOnPrimary = Color(0xFFFFFBFE)
val DarkOnSecondary = Color(0xFFFFFBFE)
val DarkOnTertiary = Color(0xFF0040FF)
val DarkOnBackground = Color.White
val DarkOnSurface = Color.White


val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)



val OceanBlue = Color(0xFF2196F3)
val SunshineYellow = Color(0xFFFFC107)
val SunsetOrange = Color(0xFFFF5722)
val GrassGreen = Color(0xFF00AA07)
val CoralRed = Color(0xFFF44336)
val White = Color(0xFFFFFFFF)
val Black = Color(0xFF000000)



val Red200 = Color(0xFFEF9A9A)
val Red500 = Color(0xFFF44336)
val Red700 = Color(0xFFD32F2F)
val Teal200 = Color(0xFF03DAC5)

val DarkBlue = Color(0xFF003366)
