package com.pomo.myapplication

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.ContextCompat

class PermissionsHelper(private val context: Context) {

    private val requiredPermissions = listOf(
        Manifest.permission.POST_NOTIFICATIONS,
        Manifest.permission.ACCESS_NOTIFICATION_POLICY,
        Manifest.permission.USE_EXACT_ALARM
    )

    private lateinit var permissionLaunchers: Map<String, ActivityResultLauncher<String>>
    var pendingPermissions: MutableList<String> = mutableListOf()


    fun initializePermissionLaunchers(
        postNotificationLauncher: ActivityResultLauncher<String>,
        accessNotificationPolicyLauncher: ActivityResultLauncher<String>,
        useExactAlarmLauncher: ActivityResultLauncher<String>
    ) {
        permissionLaunchers = mapOf(
            Manifest.permission.POST_NOTIFICATIONS to postNotificationLauncher,
            Manifest.permission.ACCESS_NOTIFICATION_POLICY to accessNotificationPolicyLauncher,
            Manifest.permission.USE_EXACT_ALARM to useExactAlarmLauncher
        )
    }


    fun checkAndRequestPermissions() {
        requiredPermissions.forEach { permission ->
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                if (!pendingPermissions.contains(permission)) {
                    pendingPermissions.add(permission)
                    permissionLaunchers[permission]?.launch(permission)
                }
            }
        }
    }

    fun requestPendingPermissions() {
        pendingPermissions.forEach { permission ->
            permissionLaunchers[permission]?.launch(permission)
        }
    }


    fun handlePermissionResult(permission: String, isGranted: Boolean) {
        if (isGranted) {
            pendingPermissions.remove(permission)
            Toast.makeText(context, "$permission Permission Granted", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "$permission Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }
}
